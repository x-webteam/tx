﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(tx.Startup))]
namespace tx
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
