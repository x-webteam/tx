﻿var app = angular.module('tx.subscription', ['ngAnimate', 'ui.router', 'mm.iban', 'resettableForm', 'angular.filter', 'angularFileUpload', 'anim-in-out']);

var path = tx.base + 'web/partials/subscription';

app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function ($stateProvider, $urlRouterProvider, $httpProvider) {

    $urlRouterProvider.otherwise('/payment');

    $stateProvider
        .state('payment', {
            url: '/payment',            
            private: false,
            views: {
                'mainView': {
                    templateUrl: path + '/payment.html',
                    controller: 'PaymentCtrl'
                }
            }
        }).state('plan', {
            url: '/plan',
            controller: 'PlanCtrl',
            private: false,
            views: {
                'mainView': {
                    templateUrl: path + '/plan.html',
                    controller: 'PlanCtrl'
                }
            }
        });

    //$httpProvider.defaults.headers.common['__RequestVerificationToken'] = tx.xsrf;
    $httpProvider.defaults.headers.common['X-XSRF-TOKEN'] = tx.xsrf;
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

    //$httpProvider.defaults.xsrfHeaderName = 'RequestVerificationToken';
    //$httpProvider.defaults.xsrfCookieName = 'cid';


}]).run(['$rootScope', function ($rootScope) {
    $rootScope.work = function (global) {
        if (global) {
            w(1);
        }
        $rootScope.working = true;
    };

    $rootScope.stop = function () {
        w(0);
        $rootScope.working = false;
    };
}]);