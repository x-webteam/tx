﻿app.filter('moment', function () {
    return function (input, format) {
        return moment(input).format(format || null);
    }
}).filter('abs', function () {
    return function (input) {
        return input < 0 ? -input : input;
    };
}).filter('ago', function () {
    return function (input) {
        return moment(input).fromNow();
    }
});