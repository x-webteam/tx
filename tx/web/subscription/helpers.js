﻿/**
 * Split and trim values
 */
function split(str, seperator) {
    if (!str) {
        return [];
    }
    return str.split('/').map(function (r) { return r.trim() }).filter(function (r) { return r.length; });
}