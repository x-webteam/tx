﻿app.controller('PaymentCtrl', ['$scope', '$http', '$location', 'Subscription', function ($scope, $http, $location, Subscription) {



    $scope.offline = (typeof Stripe === "undefined");

    $scope.refresh = function () {
        window.location.href = tx.base + 'subscription';
    };

    if ($scope.offline) {

        t('Please make sure you are connected to the internet');

        return;

    }



    Stripe.setPublishableKey('pk_test_G6QJUnx8CWavsjqQeRRCyrMC');

    $scope.number = '4242424242424242';
    $scope.cvc = '123';
    $scope.expiry = '12/2018';

    $scope.submit = function () {

        var exp = split($scope.expiry, '/');
        var exp_month = exp[0];
        var exp_year = exp[1] || "";

        var data = {
            number: $scope.number,
            cvc: $scope.cvc,
            exp_month: exp_month,
            exp_year: exp_year,
            name: $scope.name,
            address_line1: $scope.address
        };


        if (!Stripe.card.validateCardNumber(data.number)) {
            t('Your card number looks invalid');
            return false;
        }

        if (!Stripe.card.validateCVC(data.cvc)) {
            t('Your CVC looks invalid');
            return false;
        }

        if (!Stripe.card.validateExpiry(data.exp_month, data.exp_year)) {
            t('Your expiry date looks invalid');
            return false;
        }



        $scope.work(true);

        Stripe.card.createToken(data, function (status, response) {
            console.log(response);

            $scope.$apply(function () {
                $scope.stop();
            });

            if (response.error) {

                t(response.error.message);

            } else {
                // token contains id, last4, and card type
                var token = response.id;
                Subscription.setToken(token);
                $scope.$apply(function () {
                    $location.path('plan');
                });
            }
        });
    };

    setTimeout(function () {
        $('.cc-num').payment('formatCardNumber');
        //$('[data-numeric]').payment('restrictNumeric');
        $('.cc-exp').payment('formatCardExpiry');
        $('.cc-cvc').payment('formatCardCVC');
    }, 100);


}]);

app.controller('PlanCtrl', ['$scope', '$location', 'Subscription', function ($scope, $location, Subscription) {

    if (!Subscription.getToken()) {
        $location.path('payment');
    }

    $scope.setPlan = function (v) {
        $scope.plan = v;
    };

    $scope.isPlan = function (v) {
        return v === $scope.plan;
    };

    $scope.selectPlan = function (v) {

        $scope.work(true);

        Subscription.setPlan(v);
        Subscription.save().then(function (response) {
            t('Customer Created');
            setTimeout(function () {
                window.location.href = tx.base;
            }, 1200);
            
            $scope.stop();
        }, function (err) {
            $scope.stop();
            t('Operation failed');
        });

    };

}]);