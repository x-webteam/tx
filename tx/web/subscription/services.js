﻿app.factory('Subscription', ['$http', '$q', function ($http, $q) {

    var data = {};

    return {

        setToken: function (token) {
            data.token = token;
        },

        setPlan: function(plan){
            data.plan = plan
        },

        getToken: function(){
            return data.token;
        },

        getPlan: function () {
            return data.plan
        },

        save: function () {
            return $http.post('subscription', data).then(function (response) {
                console.log(response);
                if (response.data.status) {
                    return response;
                } else {
                    return $q.reject('Operation failed');
                }

            }, function (err) {
                return $q.reject(err);
            });
        }
    };
}]);