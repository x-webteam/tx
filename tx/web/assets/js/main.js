﻿var $body = $('body');
var workingTimer = null;

function t(message, duration) {
    var duration = duration || 1200;
    toast(message, duration);
}

function w(signal, duration) {
    var duration = duration || 200;

    if (workingTimer) {
        clearTimeout(workingTimer);
    }

    if (signal) {
        $body.addClass('working-element');
        workingTimer = setTimeout(function () {
            $body.addClass('working');
        }, duration);
    } else {
        $body.removeClass('working');
        workingTimer = setTimeout(function () {
            $body.removeClass('working-element');
        }, duration + 200)
    }
}

window.addEventListener('load', function () {
  

    function updateOnlineStatus(event) {
        var condition = navigator.onLine ? "online" : "offline";
        if (condition === 'online') {
            t('You are connected');
        } else {
            t('You are offline');
        }
    }

    window.addEventListener('online', updateOnlineStatus);
    window.addEventListener('offline', updateOnlineStatus);
});


$('.brand-logo').transition({ y: '0' }, 500, 'easeInOutCirc');


(function ($) {
    $(function () {

        $('.button-collapse').sideNav();

    }); // end of document ready
})(jQuery); // end of jQuery name space