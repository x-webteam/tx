﻿using System.Web;
using System.Web.Optimization;

namespace tx
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        { 

            bundles.Add(new StyleBundle("~/web/assets/base.css").Include(
                "~/web/assets/css/font-awesome.css",
                "~/web/assets/css/style.css"
                      ));

            bundles.Add(new ScriptBundle("~/web/assets/scripts.js").Include(
               "~/web/assets/js/jquery-{version}.js",
               "~/web/assets/js/materialize.js",
               "~/web/assets/js/moment.js",
               "~/web/assets/js/lodash.js",
               "~/web/assets/js/jquery.transit.js",
               "~/web/assets/js/main.js"
               ));

            bundles.Add(new ScriptBundle("~/web/assets/charts.js").Include(
                "~/web/assets/js/Chart.js"
                ));

            bundles.Add(new ScriptBundle("~/web/assets/angular.js").Include( 
               "~/web/assets/js/angular/angular.js",
               "~/web/assets/js/angular/angular-animate.js",
               "~/web/assets/js/angular/ng-iban.js",
               "~/web/assets/js/angular/angular-filter.js",
               "~/web/assets/js/angular/angular-file-upload.js",
               "~/web/assets/js/angular/ng-form-reset.js",
               "~/web/assets/js/angular/angular-ui-router.js",
               "~/web/assets/js/angular/anim-in-out.js"
               ));

            bundles.Add(new ScriptBundle("~/web/subscription.js").Include(
                "~/web/assets/js/jquery.payment.js",
                "~/web/subscription/helpers.js",
                "~/web/subscription/app.js",
                "~/web/subscription/router.js",
                "~/web/subscription/controllers.js",
                "~/web/subscription/services.js",
                "~/web/subscription/filters.js",
                "~/web/subscription/directives.js"
                ));



            /*
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
             */
        }
    }
}
