﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace tx.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            if (this.LoggedIn) {
                if (!this.ApplicationUser.isSubscribed()) {
                    return RedirectToAction("Index", "subscription");
                }
            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}