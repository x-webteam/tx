﻿using Microsoft.AspNet.Identity;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using tx.Models;

namespace tx.Controllers
{
    public class BaseController : Controller
    {

        public BaseController()
            : base()
        {
            
            this.ApplicationDbContext = new ApplicationDbContext();
            this.UserManager = new UserManager<ApplicationUser, int>(new CustomUserStore(this.ApplicationDbContext));
            this.Log = LogManager.GetLogger(GetType().FullName);

        }

        protected bool LoggedIn
        {
            get
            {
                return this.Request.IsAuthenticated;
            }
        }

        protected int UserId
        {
            get
            {
                return this.LoggedIn ? User.Identity.GetUserId<int>() : 0;
            }
        }

        protected ApplicationUser ApplicationUser
        {
            get
            {
                return this.LoggedIn ? UserManager.FindById(this.UserId) : null;
            }
        }

        protected Logger Log { get; private set; }


        /// <summary>
        /// Application DB context
        /// </summary>
        protected ApplicationDbContext ApplicationDbContext { get; set; }


        /// <summary>
        /// User manager - attached to application DB context
        /// </summary>
        protected UserManager<ApplicationUser, int> UserManager { get; set; }

        protected JsonResult Json(object json)
        {
            return Json(json, JsonRequestBehavior.AllowGet);
        }

    }
}