﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace tx.Controllers
{
    [RoutePrefix("com")]
    public class CustomController : BaseController
    {
        // GET: Custom
        [Route("in")]
        public JsonResult Index()
        {
            var result = new int[] { 1, 2, 3, 4, 5, 6 };
            return Json(result);
        }
    }
}