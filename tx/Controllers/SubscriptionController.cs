﻿using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tx.Models;
using Microsoft.AspNet.Identity;
using tx.Attributes;

namespace tx.Controllers
{

    [Authorize]
    public class SubscriptionController : BaseController
    {

        public SubscriptionController()
            : base()
        {

        }


        public ActionResult Index()
        {
            Log.Info("Going to payment");
            return View();
        }

        [HttpPost]

        //[ValidateAntiForgeryToken]
        [AngularAntiForgeryTokenAttribute]
        public ActionResult Index(string token, string plan)
        {
            var myCustomer = new StripeCustomerCreateOptions();

            // set these properties 
            // if it makes you happy

            myCustomer.Email = ApplicationUser.Email;
            myCustomer.Description = "Johnny Tenderloin (pork@email.com)";
            myCustomer.PlanId = plan;
            myCustomer.Card = new StripeCreditCardOptions();
            myCustomer.Card.TokenId = token;

            var customerService = new StripeCustomerService();

            ApplicationUser.StripeToken = token;
            ApplicationUser.ExpiryDate = DateTime.UtcNow.AddDays(30);
            ApplicationDbContext.SaveChanges();

            try
            {
                StripeCustomer stripeCustomer = customerService.Create(myCustomer);
                Log.Info("Subscription done for {0} with token: {1}, plan: {2}", myCustomer.Email, token, plan);

                return Json(new { status = true });
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return Json(new
                {
                    status = false
                });
            }
        }
    }
}