﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tx.Models
{
    public class BaseModel
    {
        public Guid TenantId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
    }
}