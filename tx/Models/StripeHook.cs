﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tx.Models
{
    public class StripeHook:BaseModel
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string Payload { get; set; }
    }
}